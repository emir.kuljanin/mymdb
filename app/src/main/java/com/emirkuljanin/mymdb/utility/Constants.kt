package com.emirkuljanin.mymdb.utility

//API Constants
const val BASE_URL = "https://api.themoviedb.org/3/"
const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w400%s"
const val MOVIE_API_KEY = "cd49a013f280a1cd8ad261e5be19ede7"
const val QUERY_API_KEY = "api_key"
const val QUERY_PAGE = "page"
const val ID_PATH = "id"
const val ERROR = "Error"

//Movie types
const val MOVIE_TYPE_TOP_RATED = "top_rated"
const val MOVIE_TYPE_POPULAR = "popular"
const val MOVIE_TYPE_SIMILAR = "similar"

//Movie list/details
const val MOVIE_STARTING_PAGE = 1
const val MOVIE_ID_KEY = "movieId"