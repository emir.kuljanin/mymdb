package com.emirkuljanin.mymdb.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.emirkuljanin.mymdb.R
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)

        //Launch MainActivity after 2 seconds
        lifecycleScope.launch {
            delay(2_000)
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
        }
    }
}