package com.emirkuljanin.mymdb.ui.moviedetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.emirkuljanin.mymdb.data.repository.MovieRepository

class MovieDetailsViewModelFactory : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MovieDetailsViewModel(MovieRepository) as T
    }
}