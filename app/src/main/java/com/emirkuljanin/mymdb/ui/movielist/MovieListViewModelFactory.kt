package com.emirkuljanin.mymdb.ui.movielist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.emirkuljanin.mymdb.data.repository.MovieRepository

class MovieListViewModelFactory : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MovieListViewModel(MovieRepository) as T
    }
}