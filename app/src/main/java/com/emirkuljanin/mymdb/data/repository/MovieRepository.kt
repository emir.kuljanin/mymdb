package com.emirkuljanin.mymdb.data.repository

import com.emirkuljanin.mymdb.data.api.RetrofitClient
import com.emirkuljanin.mymdb.data.model.MovieListResponse
import com.emirkuljanin.mymdb.utility.MOVIE_TYPE_POPULAR
import com.emirkuljanin.mymdb.utility.MOVIE_TYPE_TOP_RATED

object MovieRepository {

    suspend fun getMovieListForType(type: String, page: Int): MovieListResponse {
        //Change call based on type of movie list required
        when (type) {
            MOVIE_TYPE_POPULAR -> return RetrofitClient.movieInterface.getPopularMovies(page)
            MOVIE_TYPE_TOP_RATED -> return RetrofitClient.movieInterface.getTopRatedMovies(page)
        }

        return MovieListResponse(page, arrayListOf())
    }

    suspend fun getSimilarMoviesList(id: Long, page: Int) =
        RetrofitClient.movieInterface.getSimilarMovies(id, page)

    suspend fun getMovieDetails(id: Long) = RetrofitClient.movieInterface.getMovie(id)
}