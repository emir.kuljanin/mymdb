package com.emirkuljanin.mymdb.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.emirkuljanin.mymdb.data.api.MovieInterface
import com.emirkuljanin.mymdb.data.model.Movie
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class MovieServiceTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val movieId: Long = 399566
    private lateinit var service: MovieInterface
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url(""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(MovieInterface::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun getTopRatedMovies() {
        runBlocking {
            enqueueResponse("topratedmovies.json")
            val resultResponse = service.getTopRatedMovies(1)

            val request = mockWebServer.takeRequest()
            Assert.assertNotNull(resultResponse)
            Assert.assertThat(request.path, `is`("/movie/top_rated?page=1"))

            resultResponse.movieList.forEach {
                verifyMovieData(it)
            }
        }
    }

    @Test
    fun getPopularMovies() {
        runBlocking {
            enqueueResponse("popularmovies.json")
            val resultResponse = service.getPopularMovies(1)

            val request = mockWebServer.takeRequest()
            Assert.assertNotNull(resultResponse)
            Assert.assertThat(request.path, `is`("/movie/popular?page=1"))

            resultResponse.movieList.forEach {
                verifyMovieData(it)
            }
        }
    }

    @Test
    fun getMovieDetails() {
        runBlocking {
            enqueueResponse("moviedetails.json")
            val resultResponse = service.getMovie(movieId)

            val request = mockWebServer.takeRequest()
            Assert.assertNotNull(resultResponse)
            Assert.assertThat(request.path, `is`("/movie/$movieId"))

            Assert.assertNotNull(resultResponse.id)
            Assert.assertNotNull(resultResponse.title)
            Assert.assertNotNull(resultResponse.backdrop_path)
            Assert.assertNotNull(resultResponse.poster_path)
            Assert.assertNotNull(resultResponse.release_date)
            Assert.assertNotNull(resultResponse.overview)
            Assert.assertNotNull(resultResponse.genres)

            assertThat(resultResponse.id, `is`(movieId))
            assertThat(resultResponse.title, `is`("Godzilla vs. Kong"))
            assertThat(resultResponse.release_date, `is`("2021-03-24"))
            assertThat(resultResponse.overview, `is`("In a time when monsters walk the Earth, humanity’s fight for its future sets Godzilla and Kong on a collision course that will see the two most powerful forces of nature on the planet collide in a spectacular battle for the ages."))
        }
    }

    @Test
    fun getSimilarMovies() {
        runBlocking {
            enqueueResponse("similarmovies.json")
            val resultResponse = service.getSimilarMovies(movieId, 1)

            val request = mockWebServer.takeRequest()
            Assert.assertNotNull(resultResponse)
            Assert.assertThat(request.path, `is`("/movie/$movieId/similar?page=1"))

            resultResponse.movieList.forEach {
                verifyMovieData(it)
            }
        }
    }

    //Verify only data we currently need is not null
    private fun verifyMovieData(movie: Movie) {
        Assert.assertNotNull(movie.id)
        Assert.assertNotNull(movie.title)
        Assert.assertNotNull(movie.backdrop_path)
        Assert.assertNotNull(movie.poster_path)
        Assert.assertNotNull(movie.release_date)
        Assert.assertNotNull(movie.overview)
    }

    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader
            ?.getResourceAsStream("api-response/$fileName")
        val source = inputStream?.source()?.buffer()
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }

        if (source != null) {
            mockWebServer.enqueue(
                mockResponse.setBody(
                    source.readString(Charsets.UTF_8)
                )
            )
        }
    }
}