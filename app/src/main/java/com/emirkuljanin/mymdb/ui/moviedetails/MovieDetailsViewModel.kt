package com.emirkuljanin.mymdb.ui.moviedetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.emirkuljanin.mymdb.data.repository.MovieRepository
import com.emirkuljanin.mymdb.utility.ERROR
import com.emirkuljanin.mymdb.utility.network.Resource
import kotlinx.coroutines.Dispatchers

class MovieDetailsViewModel(
    private val movieRepository: MovieRepository) : ViewModel() {
    //Get movie details for movieId
    fun getMovieDetails(movieId: Long) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = movieRepository.getMovieDetails(movieId)))
        } catch (exception: Exception) {
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: ERROR
                )
            )
        }
    }

    //Get similar movies for given movieId
    fun getSimilarMovies(movieId: Long, page: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = movieRepository.getSimilarMoviesList(movieId, page)))
        } catch (exception: Exception) {
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: ERROR
                )
            )
        }
    }
}