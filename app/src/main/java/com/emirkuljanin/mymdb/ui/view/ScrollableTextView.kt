package com.emirkuljanin.mymdb.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatTextView

//Accessibility class to support touch events for visually impaired users
class ScrollableTextView : AppCompatTextView {
    constructor(context: Context?) : super(context!!)
    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs)

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        parent.requestDisallowInterceptTouchEvent(true)
        when (event.action) {
            MotionEvent.ACTION_DOWN -> return true
            MotionEvent.ACTION_UP -> {
                performClick()
                return true
            }
        }
        return false
    }

    //Because we call this from onTouchEvent, this code will be executed for both
    //normal touch events and for when the system calls this using Accessibility
    override fun performClick(): Boolean {
        super.performClick()
        parent.requestDisallowInterceptTouchEvent(false)
        return true
    }
}