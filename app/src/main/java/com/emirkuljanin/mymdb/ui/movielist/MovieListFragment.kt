package com.emirkuljanin.mymdb.ui.movielist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.emirkuljanin.mymdb.R
import com.emirkuljanin.mymdb.data.model.MovieListResponse
import com.emirkuljanin.mymdb.databinding.FragmentMovieListBinding
import com.emirkuljanin.mymdb.ui.view.MovieListView
import com.emirkuljanin.mymdb.ui.view.MovieListView.LoadMoreListener
import com.emirkuljanin.mymdb.utility.*
import com.emirkuljanin.mymdb.utility.adapter.MovieRecyclerViewAdapter
import com.emirkuljanin.mymdb.utility.adapter.MovieRecyclerViewAdapter.MovieClickListener
import com.emirkuljanin.mymdb.utility.network.Resource
import com.emirkuljanin.mymdb.utility.network.Status

class MovieListFragment : Fragment(), MovieClickListener, LoadMoreListener {
    private lateinit var viewModel: MovieListViewModel
    private lateinit var binding: FragmentMovieListBinding
    private lateinit var popularMoviesAdapter: MovieRecyclerViewAdapter
    private lateinit var topRatedMoviesAdapter: MovieRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_movie_list, container,
            false
        )

        initializeUI()

        return binding.root
    }

    private fun initializeUI() {
        val activity = activity ?: return
        val factory = InjectorUtil.provideMovieListViewModel()
        //Create or get already created CastingTaskViewModel for this view
        viewModel = ViewModelProvider(this, factory)
            .get(MovieListViewModel::class.java)

        //Initialize adapters
        popularMoviesAdapter = MovieRecyclerViewAdapter(
            activity, arrayListOf(),
            this
        )
        topRatedMoviesAdapter = MovieRecyclerViewAdapter(
            activity, arrayListOf(),
            this
        )

        //Set data to views
        binding.popularMoviesView.setData(popularMoviesAdapter, MOVIE_TYPE_POPULAR)
        binding.topRatedMoviesView.setData(topRatedMoviesAdapter, MOVIE_TYPE_TOP_RATED)

        //Set load more listeners
        binding.popularMoviesView.setLoadMoreListener(this)
        binding.topRatedMoviesView.setLoadMoreListener(this)

        //Set observers
        setObservers()
    }

    private fun setObservers() {
        val activity = activity ?: return
        //Popular movies
        viewModel.getMovieListForType(MOVIE_TYPE_POPULAR, MOVIE_STARTING_PAGE).observe(activity, {
            it?.let {
                handleMovieResponse(it, MOVIE_TYPE_POPULAR)
            }
        })

        //Top rated movies
        viewModel.getMovieListForType(MOVIE_TYPE_TOP_RATED, MOVIE_STARTING_PAGE).observe(activity, {
            it?.let {
                handleMovieResponse(it, MOVIE_TYPE_TOP_RATED)
            }
        })
    }

    //Handle response received based on type passed
    private fun handleMovieResponse(movieListResource: Resource<MovieListResponse>, type: String) {
        //Handle view and adapter based on list type
        val movieList: MovieListView
        val adapter: MovieRecyclerViewAdapter
        when (type) {
            MOVIE_TYPE_POPULAR -> {
                movieList = binding.popularMoviesView
                adapter = popularMoviesAdapter
            }

            MOVIE_TYPE_TOP_RATED -> {
                movieList = binding.topRatedMoviesView
                adapter = topRatedMoviesAdapter
            }

            else -> return
        }

        //Handle response
        when (movieListResource.status) {
            Status.SUCCESS -> {
                //Call successful, handle data and hide progress bar
                movieListResource.data?.let { movieListResponse ->
                    //Set data into adapter
                    adapter.apply {
                        addMovies(movieListResponse.movieList)
                        //If loading first page, list is empty, avoid going into negative index
                        var startPosition = adapter.getMovies().size - 1
                        if (movieListResponse.page == 1) {
                            startPosition = 0
                        }

                        notifyItemRangeInserted(startPosition, movieListResponse.movieList.size)
                    }
                }

                movieList.hideProgressBar()
            }

            Status.ERROR -> {
                //Call failed show error message
                Toast.makeText(activity, movieListResource.message, Toast.LENGTH_SHORT).show()
                movieList.hideProgressBar()
            }

            Status.LOADING -> {
                //Do nothing (progressBar is already showing)
            }
        }

        movieList.stopLoading()
    }

    //Movie was selected, open movie details and pass movieId
    override fun onMovieSelected(movieId: Long) {
        findNavController().navigate(
            R.id.openMovieDetails,
            bundleOf(MOVIE_ID_KEY to movieId)
        )
    }

    //Load more movies based on list movieType
    override fun loadMore(pageToLoad: Int, movieType: String) {
        val activity = activity ?: return

        viewModel.getMovieListForType(movieType, pageToLoad).observe(activity, {
            it?.let {
                //Handle response
                handleMovieResponse(it, movieType)
            }
        })
    }
}