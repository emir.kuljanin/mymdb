package com.emirkuljanin.mymdb.utility

import com.emirkuljanin.mymdb.ui.moviedetails.MovieDetailsViewModelFactory
import com.emirkuljanin.mymdb.ui.movielist.MovieListViewModelFactory

//The whole dependency tree is constructed right here
object InjectorUtil {

    fun provideMovieListViewModel(): MovieListViewModelFactory {
        return MovieListViewModelFactory()
    }

    fun provideMovieDetailsViewModel(): MovieDetailsViewModelFactory {
        return MovieDetailsViewModelFactory()
    }
}