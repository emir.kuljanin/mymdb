package com.emirkuljanin.mymdb.utility.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.emirkuljanin.mymdb.data.model.Genre
import com.emirkuljanin.mymdb.databinding.GenreRecyclerItemBinding

class GenreRecyclerViewAdapter(
    private val context: Context,
    private val genreList: ArrayList<Genre>
) :
    RecyclerView.Adapter<GenreRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = GenreRecyclerItemBinding.inflate(
            LayoutInflater.from(context), parent,
            false
        )

        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val genre = genreList[position]

        holder.binding.genreName = genre.name
    }

    override fun getItemCount(): Int {
        return genreList.size
    }

    fun addGenres(genreList: List<Genre>) {
        this.genreList.addAll(genreList)
    }

    inner class ViewHolder(val binding: GenreRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}