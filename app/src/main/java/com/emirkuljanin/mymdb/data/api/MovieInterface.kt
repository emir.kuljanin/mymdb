package com.emirkuljanin.mymdb.data.api

import com.emirkuljanin.mymdb.data.model.MovieDetails
import com.emirkuljanin.mymdb.data.model.MovieListResponse
import com.emirkuljanin.mymdb.utility.ID_PATH
import com.emirkuljanin.mymdb.utility.QUERY_PAGE
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieInterface {

    @GET("movie/popular")
    suspend fun getPopularMovies(@Query(QUERY_PAGE) page: Int): MovieListResponse

    @GET("movie/top_rated")
    suspend fun getTopRatedMovies(@Query(QUERY_PAGE) page: Int): MovieListResponse

    @GET("movie/{$ID_PATH}")
    suspend fun getMovie(@Path(ID_PATH) id: Long): MovieDetails

    @GET("movie/{$ID_PATH}/similar")
    suspend fun getSimilarMovies(
        @Path(ID_PATH) id: Long,
        @Query(QUERY_PAGE) page: Int
    ): MovieListResponse
}