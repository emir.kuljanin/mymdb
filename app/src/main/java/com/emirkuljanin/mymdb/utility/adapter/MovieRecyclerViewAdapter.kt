package com.emirkuljanin.mymdb.utility.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.emirkuljanin.mymdb.data.model.Movie
import com.emirkuljanin.mymdb.databinding.MovieRecyclerItemBinding
import com.emirkuljanin.mymdb.utility.IMAGE_BASE_URL

class MovieRecyclerViewAdapter(
    private val context: Context,
    private val movieList: ArrayList<Movie>,
    private val movieClickListener: MovieClickListener
) :
    RecyclerView.Adapter<MovieRecyclerViewAdapter.ViewHolder>() {
    interface MovieClickListener {
        fun onMovieSelected(movieId: Long)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MovieRecyclerViewAdapter.ViewHolder {
        val itemBinding = MovieRecyclerItemBinding.inflate(
            LayoutInflater.from(context), parent,
            false
        )

        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = movieList[position]
        holder.binding.movieTitle = movie.title

        //Generate image URL (Image base URL + poster path from movie object)
        val imageURL = String.format(IMAGE_BASE_URL, movie.poster_path)

        //Create load listener and hide progress bar after loading is finished
        val loadListener = object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                holder.binding.progressBar.visibility = View.GONE
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                holder.binding.progressBar.visibility = View.GONE
                return false
            }
        }

        //Load image into view
        Glide.with(context)
            .load(imageURL)
            .listener(loadListener)
            .into(holder.binding.movieImageView)
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    fun addMovies(movieList: List<Movie>) {
        this.movieList.addAll(movieList)
    }

    fun getMovies(): List<Movie> {
        return movieList
    }

    inner class ViewHolder(val binding: MovieRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        //Add on click listener to item, and notify fragment
        init {
            binding.root.setOnClickListener {
                movieClickListener.onMovieSelected(movieList[adapterPosition].id)
            }
        }
    }
}