package com.emirkuljanin.mymdb.data.api

import com.emirkuljanin.mymdb.BuildConfig
import com.emirkuljanin.mymdb.utility.BASE_URL
import com.emirkuljanin.mymdb.utility.MOVIE_API_KEY
import com.emirkuljanin.mymdb.utility.QUERY_API_KEY
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    private fun getRetrofitBuilder(): Retrofit.Builder {
        //Log only if working on debug version
        val levelType = if (BuildConfig.DEBUG) Level.BODY else Level.NONE

        //Create log interceptor
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(levelType)

        //Create query interceptor
        val queryInterceptor = object : Interceptor {
            //Intercept URL and add api key and language query values
            override fun intercept(chain: Interceptor.Chain): Response {
                val original: Request = chain.request()
                val originalHttpUrl: HttpUrl = original.url

                //Add API Key to query
                val url = originalHttpUrl.newBuilder()
                    .addQueryParameter(QUERY_API_KEY, MOVIE_API_KEY)
                    .build()

                val requestBuilder: Request.Builder = original.newBuilder().url(url)
                val request: Request = requestBuilder.build()
                return chain.proceed(request)
            }
        }

        //Set client
        val okHttpClient = OkHttpClient.Builder()
        okHttpClient.addInterceptor(loggingInterceptor)

        //Add query interceptor for client
        okHttpClient.addInterceptor(queryInterceptor)

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient.build())
            .addConverterFactory(GsonConverterFactory.create())
    }

    //Create lazy interface for movies
    val movieInterface: MovieInterface by lazy {
        getRetrofitBuilder()
            .build()
            .create(MovieInterface::class.java)
    }
}