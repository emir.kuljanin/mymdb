package com.emirkuljanin.mymdb.ui.movielist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.emirkuljanin.mymdb.data.repository.MovieRepository
import com.emirkuljanin.mymdb.utility.ERROR
import com.emirkuljanin.mymdb.utility.network.Resource
import kotlinx.coroutines.Dispatchers

class MovieListViewModel(private val movieRepository: MovieRepository) : ViewModel() {
    //Get movie list for passed list type
    fun getMovieListForType(type: String, page: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = movieRepository.getMovieListForType(type, page)))
        } catch (exception: Exception) {
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: ERROR
                )
            )
        }
    }
}