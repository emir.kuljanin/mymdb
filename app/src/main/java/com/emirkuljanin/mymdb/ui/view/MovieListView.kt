package com.emirkuljanin.mymdb.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.emirkuljanin.mymdb.R
import com.emirkuljanin.mymdb.databinding.MovieListViewBinding
import com.emirkuljanin.mymdb.utility.MOVIE_STARTING_PAGE
import com.emirkuljanin.mymdb.utility.MOVIE_TYPE_POPULAR
import com.emirkuljanin.mymdb.utility.MOVIE_TYPE_SIMILAR
import com.emirkuljanin.mymdb.utility.MOVIE_TYPE_TOP_RATED
import com.emirkuljanin.mymdb.utility.adapter.MovieRecyclerViewAdapter

class MovieListView : LinearLayout {
    interface LoadMoreListener {
        fun loadMore(pageToLoad: Int, movieType: String)
    }

    lateinit var binding: MovieListViewBinding
    private var isLoading = true
    private var loadMoreListener: LoadMoreListener? = null
    private var currentPage = MOVIE_STARTING_PAGE

    constructor(context: Context) : super(context) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initialize(context)
    }

    //Create view binding
    private fun initialize(context: Context) {
        binding = MovieListViewBinding.inflate(
            LayoutInflater.from(context), this,
            true
        )
    }

    //Set movie list and create adapter
    fun setData(adapter: MovieRecyclerViewAdapter, movieType: String) {
        binding.headerText = getHeaderTitleForMovieType(movieType)

        //Set Horizontal layout manager
        val layoutManager = LinearLayoutManager(
            context, RecyclerView.HORIZONTAL,
            false
        )

        binding.layoutManager = layoutManager

        //Create recycler adapter
        binding.moviesRecyclerView.adapter = adapter

        //Set on scroll listener
        binding.moviesRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                //If currently loading, or there is no interface set, return
                if (isLoading || loadMoreListener == null) {
                    return
                }

                //If scrolled to last item, notify interface to load more movies
                if (layoutManager.findLastCompletelyVisibleItemPosition() ==
                    adapter.getMovies().size - 1
                ) {
                    //Send page number that should be loaded
                    currentPage++
                    loadMoreListener?.loadMore(currentPage, movieType)
                    isLoading = true
                }
            }
        })
    }

    //Set LoadMoreListener
    fun setLoadMoreListener(loadMoreListener: LoadMoreListener?) {
        this.loadMoreListener = loadMoreListener
    }

    //Loading finished for endless scrolling
    fun stopLoading() {
        isLoading = false
    }

    //Hide progress bar and show list after loading is done
    fun hideProgressBar() {
        binding.moviesRecyclerView.visibility = View.VISIBLE
        binding.movieProgressBar.visibility = View.GONE
    }

    //Return translated string for specified movie type
    private fun getHeaderTitleForMovieType(movieType: String): String {
        return when (movieType) {
            MOVIE_TYPE_POPULAR -> context.getString(R.string.popular_movies)
            MOVIE_TYPE_TOP_RATED -> context.getString(R.string.top_rated_movies)
            MOVIE_TYPE_SIMILAR -> context.getString(R.string.similar_movies)
            else -> ""
        }
    }
}