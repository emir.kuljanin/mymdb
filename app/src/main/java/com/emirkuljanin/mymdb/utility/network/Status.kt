package com.emirkuljanin.mymdb.utility.network

//API call status
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}