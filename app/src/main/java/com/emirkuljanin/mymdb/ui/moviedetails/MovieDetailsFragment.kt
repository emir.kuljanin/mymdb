package com.emirkuljanin.mymdb.ui.moviedetails

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.emirkuljanin.mymdb.R
import com.emirkuljanin.mymdb.databinding.FragmentMovieDetailsBinding
import com.emirkuljanin.mymdb.ui.view.MovieListView.LoadMoreListener
import com.emirkuljanin.mymdb.utility.*
import com.emirkuljanin.mymdb.utility.adapter.GenreRecyclerViewAdapter
import com.emirkuljanin.mymdb.utility.adapter.MovieRecyclerViewAdapter
import com.emirkuljanin.mymdb.utility.adapter.MovieRecyclerViewAdapter.MovieClickListener
import com.emirkuljanin.mymdb.utility.network.Status

class MovieDetailsFragment : Fragment(), MovieClickListener, LoadMoreListener {
    private lateinit var viewModel: MovieDetailsViewModel
    private lateinit var binding: FragmentMovieDetailsBinding
    private lateinit var similarMovieAdapter: MovieRecyclerViewAdapter
    private lateinit var genreAdapter: GenreRecyclerViewAdapter

    private var movieId: Long? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_movie_details, container,
            false
        )

        //Read movieId from arguments
        arguments?.getLong(MOVIE_ID_KEY)?.let {
            movieId = it
        }

        initializeUI()

        return binding.root
    }

    private fun initializeUI() {
        val activity = activity ?: return
        val factory = InjectorUtil.provideMovieDetailsViewModel()
        //Create or get already created CastingTaskViewModel for this view
        viewModel = ViewModelProvider(this, factory)
            .get(MovieDetailsViewModel::class.java)

        //Initialize adapters

        //Genre adapter
        genreAdapter = GenreRecyclerViewAdapter(activity, arrayListOf())
        //Similar movies adapter
        similarMovieAdapter = MovieRecyclerViewAdapter(
            activity,
            arrayListOf(),
            this
        )

        //Set layout manager and adapter to genre recycler view
        //Set Horizontal layout manager
        binding.layoutManager = LinearLayoutManager(
            context, RecyclerView.HORIZONTAL,
            false
        )

        //Create recycler adapter
        binding.movieGenreRecyclerView.adapter = genreAdapter

        //Set data to similar movies list
        binding.similarMoviesView.setData(similarMovieAdapter, MOVIE_TYPE_SIMILAR)

        setObservers()
    }

    private fun setObservers() {
        val activity = activity ?: return

        //Make val in order to remove mutability from movieId to prevent value changing
        val nonMutableMovieId = movieId

        //If movieId == null return to previous screen in order to prevent unwanted behavior
        if (nonMutableMovieId == null) {
            activity.onBackPressed()
            return
        }

        //Movie details
        setMovieDetailsObserver(nonMutableMovieId)

        //Similar movies
        setSimilarMoviesObserver(nonMutableMovieId)
    }

    //Get movie details
    private fun setMovieDetailsObserver(movieId: Long) {
        val activity = activity ?: return

        viewModel.getMovieDetails(movieId).observe(activity, {
            it?.let {
                //Handle response
                when (it.status) {
                    Status.SUCCESS -> {
                        //Call successful, handle data and hide progress bar
                        val movie = it.data ?: return@observe
                        binding.movie = movie

                        //Generate image URL (Image base URL + image path from movie object)
                        val coverURL = String.format(IMAGE_BASE_URL, movie.backdrop_path)
                        val posterURL = String.format(IMAGE_BASE_URL, movie.poster_path)

                        //Load images with glide
                        Glide.with(activity)
                            .load(coverURL)
                            .into(binding.movieCoverImageView)

                        Glide.with(activity)
                            .load(posterURL)
                            .into(binding.moviePosterImageView)

                        //Set genre data
                        genreAdapter.apply {
                            addGenres(movie.genres)
                            notifyDataSetChanged()
                        }

                        //Make overview text view scrollable
                        binding.movieOverviewTextView.movementMethod = ScrollingMovementMethod()

                        binding.movieDetailsProgressBar.visibility = View.GONE
                    }

                    Status.ERROR -> {
                        //Call failed show error message
                        binding.movieDetailsProgressBar.visibility = View.GONE
                        Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show()
                    }

                    Status.LOADING -> {
                        //Show progress bar
                        binding.movieDetailsProgressBar.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    //Get similar movies and observe result
    private fun setSimilarMoviesObserver(movieId: Long) {
        val activity = activity ?: return

        viewModel.getSimilarMovies(movieId, MOVIE_STARTING_PAGE).observe(activity, {
            it?.let {
                //Handle response
                when (it.status) {
                    Status.SUCCESS -> {
                        //Call successful, handle data and hide progress bar
                        it.data?.let { movieListResponse ->
                            //Set data into adapter
                            similarMovieAdapter.apply {
                                addMovies(movieListResponse.movieList)
                                notifyDataSetChanged()
                            }
                        }

                        binding.similarMoviesView.hideProgressBar()
                    }

                    Status.ERROR -> {
                        //Call failed show error message
                        binding.similarMoviesView.hideProgressBar()
                        Toast.makeText(activity, it.message, Toast.LENGTH_SHORT).show()
                    }

                    Status.LOADING -> {
                        //Do nothing (progressBar is already showing)
                    }
                }
                binding.similarMoviesView.stopLoading()
            }
        })
    }

    //Similar movie selected, open details for that movie
    override fun onMovieSelected(movieId: Long) {
        //Remove current details screen
        findNavController().popBackStack(R.id.movieDetailsFragment, true)

        //Open new details screen
        findNavController().navigate(
            R.id.openMovieDetails,
            bundleOf(MOVIE_ID_KEY to movieId)
        )
    }

    //Load more movies
    override fun loadMore(pageToLoad: Int, movieType: String) {
        //Supposed to implement pagination, but every page returns the same set of movies
        //from the API, so leave blank for now (pagination will be implemented on movie list screen
        //as a PoC) i.e this link, every page returns the same results. (also movieId 791373)
        //https://api.themoviedb.org/3/movie/399566/similar?api_key=fe3b8cf16d78a0e23f0c509d8c37caad&page=1
    }
}